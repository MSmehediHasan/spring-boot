# Datasource configuration properties
spring.datasource.url=jdbc:mysql://localhost:3306/demo?allowPublicKeyRetrieval=true&useSSL=false
spring.datasource.username=root
spring.datasource.password=open
spring.datasource.driver-class-name=com.mysql.jdbc.Driver

# Spring Data JPA configuration properties
spring.jpa.database-platform=org.hibernate.dialect.MySQL5Dialect
spring.thymeleaf.cache=true
spring.jpa.show-sql=true
spring.jpa.generate-ddl=true
spring.jpa.hibernate.ddl-auto=update
spring.jpa.properties.hibernate.default_schema=public